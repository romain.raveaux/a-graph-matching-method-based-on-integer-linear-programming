### A graph matching method based on Integer Linear Programming

Goals :

*   An introduction to Graph Matching.
*   The graph matching problem is defined
*   An integer linear program is presented
*   A solver based on the integer linear program is implemented
*   An application to Letter matching is proposed.
*   Code in Python base Numpy, Matplotlib and Gurobi is proposed through a Jupyter Notebook.

The tutorial can be found here. [HTML Notebook](http://romain.raveaux.free.fr/document/A_graph_matching_method_based_ILP.html) ![](http://romain.raveaux.free.fr/document/subgm.png) ![](http://romain.raveaux.free.fr/document/F2image.png)
